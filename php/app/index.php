<?php

$host = 'db';
$db   = 'dockerphp';
$user = 'sqluser';
$pass = 'sqlpassword';
$port = "3306";
$charset = 'utf8mb4';

$options = [
    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    \PDO::ATTR_EMULATE_PREPARES   => false,
];

$dsn = "mysql:host=$host;dbname=$db;charset=$charset;port=$port";

try {
     $pdo = new \PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

if(!tableExists($pdo, "HelloWorld"))
{
    createTables($pdo);
}
       
$sql = $pdo->prepare("SELECT * FROM HelloWorld");
$sql->execute();

$result = $sql->fetch(PDO::FETCH_ASSOC);
echo $result['greeting'];


function tableExists($pdo, $table) {
    try {
        $result = $pdo->query("SELECT 1 FROM $table LIMIT 1");
    } catch (Exception $e) {
        return FALSE;
    }
    return $result !== FALSE;
}

function createTables($pdo){  
    try{
        $sql = "CREATE TABLE HelloWorld (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            greeting VARCHAR(200) NOT NULL
        )";

        $pdo->exec($sql);

        $sql = "INSERT INTO HelloWorld (id, greeting) VALUES (0, 'Hello World PHP + MySQL')";
        $pdo->exec($sql);

    } catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
}